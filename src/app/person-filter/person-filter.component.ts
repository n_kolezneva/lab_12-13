import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-person-filter',
  templateUrl: './person-filter.component.html',
  styleUrls: ['./person-filter.component.css']
})
export class PersonFilterComponent implements OnInit {

  @Output() filterPersonsList = new EventEmitter<object>();

  searching = {
    firstName: ''
  };

  constructor() {
  }

  ngOnInit() {
  }

  onFilterPersonsList() {
    this.filterPersonsList.emit(this.searching);
  }
}
