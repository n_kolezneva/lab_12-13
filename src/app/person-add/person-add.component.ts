import {Component, Output, EventEmitter, OnInit} from '@angular/core';
import {Person} from '../shared/models/person.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';


@Component({
  selector: 'app-person-add',
  templateUrl: './person-add.component.html',
  styleUrls: ['./person-add.component.css']
})
export class PersonAddComponent implements OnInit {

  @Output() addPerson = new EventEmitter<Person>();
  addPersonForm: FormGroup;
  disabledControl: boolean;

  constructor() {
  }

  ngOnInit(): void {
    this.addPersonForm = new FormGroup({
      firstName: new FormControl({value: '', disabled: this.disabledControl}, [Validators.required]),
      lastName: new FormControl({value: '', disabled: this.disabledControl}, [Validators.required]),
      tel: new FormControl({value: '', disabled: this.disabledControl}, [Validators.required]),
    });

  }

  onAddPerson() {
    const person = this.addPersonForm.value;
    this.addPerson.emit(person);
    this.addPersonForm.reset();
  }

}
