import {Component, OnDestroy, OnInit} from '@angular/core';
import {Person} from './shared/models/person.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  order: string = 'info.name';
  reverse: boolean = false;
  title = 'Список';
  persons: Person[] = [];

  searching: object = {
    firstName: '',
    lastName: ''
  };

  ngOnInit(): void {
    this.persons.push(new Person('Надежда', '2020-01-10', '2020-01-17', 1));
    this.persons.push(new Person('Пелагея', '2020-01-03', 'a', 2));
    this.persons.push(new Person('Валерий', '2020-01-07', '2020-03-11', 3));
    this.persons.push(new Person('Никола', '2020-01-08', '2020-01-11', 4));
    this.persons.push(new Person('Баша', '2020-01-12', '2020-08-11', 5));
    this.persons.push(new Person('Григорий', '2016-01-11', '2020-11-11', 6));
    this.persons.push(new Person('Славянка', '2010-01-11', '2020-05-11', 6));
  }

  onAddPerson(person: Person) {
    person.id = (this.persons.length !== 0) ? this.persons[this.persons.length - 1].id + 1 : 1;
    this.persons.push(person);
  }

  onRemovePerson(id: number) {
    this.persons = this.persons.filter(person => person.id !== id);
  }

  onFilterPersonsList(e: object) {
    this.searching = e;
  }
  sortedCollection: Person[];
  
  constructor(private orderPipe: OrderPipe) {
    this.sortedCollection = orderPipe.transform(this.collection, 'info.name');
    console.log(this.sortedCollection);
  }

  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }

    this.order = value;
  }
}
