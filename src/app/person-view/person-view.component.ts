import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Person} from '../shared/models/person.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-person-view',
  templateUrl: './person-view.component.html',
  styleUrls: ['./person-view.component.css']
})
export class PersonViewComponent implements OnInit {
  date_out='2020-01-17';

  @Input() inPerson: Person;

  @Output() removePerson = new EventEmitter<number>();

  editActive = false;

  editPersonForm: FormGroup;
  disabledControl: boolean;

  constructor() {
  }

  ngOnInit(): void {
    this.editPersonForm = new FormGroup({
      firstName: new FormControl({value: '', disabled: this.disabledControl}, [Validators.required]),
      lastName: new FormControl({value: '', disabled: this.disabledControl}, [Validators.required]),
      tel: new FormControl({value: '', disabled: this.disabledControl}, [Validators.required]),
    });

  }

  onRemovePerson(id: number) {
    this.removePerson.emit(id);
  }

  onEditPerson(isOpening: boolean) {
    if (isOpening) {
      this.editActive = true;

      const formValues = this.editPersonForm.controls;

      for (let key in formValues)
        this.editPersonForm.controls[key].setValue(this.inPerson[key]);


    } else {
      this.editActive = false;
      const formValues = this.editPersonForm.controls;

      for (let key in formValues)

          this.inPerson[key] = formValues[key].value;
  
    }
  }

/*
  checkValue(e) {
    let value = e.target.value;
  }*/

}
