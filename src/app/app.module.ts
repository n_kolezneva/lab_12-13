import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { OrderModule } from 'ngx-order-pipe';
import { PersonViewComponent } from './person-view/person-view.component';
import { PersonAddComponent } from './person-add/person-add.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {NgxMaskModule} from 'ngx-mask';
import { FilterPersonsPipe } from './shared/pipes/filter-persons.pipe';
import { PersonFilterComponent } from './person-filter/person-filter.component';

@NgModule({
  declarations: [
    AppComponent,
    PersonViewComponent,
    PersonAddComponent,
    FilterPersonsPipe,
    PersonFilterComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    OrderModule,
    NgxMaskModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

}


